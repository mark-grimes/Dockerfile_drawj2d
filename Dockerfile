FROM alpine as builder

ARG DRAWJ2D_VERSION="1.3.4"

# Adding apache-ant will bring in java as a dependency
RUN apk add --no-cache apache-ant curl

RUN mkdir /build && \
	cd /build && \
	curl -L "https://downloads.sourceforge.net/project/drawj2d/${DRAWJ2D_VERSION}/Drawj2d-${DRAWJ2D_VERSION}.src.tar.gz" | tar -xz

RUN cd /build/Drawj2d && \
	ant

# The shebang in the start script uses bash, but alpine doesn't have bash installed by default
RUN sed -i s:"/bin/bash":"/bin/sh": /build/Drawj2d/dist/drawj2d
# For some reason the start script isn't executable by default either
RUN chmod a+x /build/Drawj2d/dist/drawj2d

#
# Now have a base image with just basic Java JRE. Note that we can't use openjdk17-jre-headless
# because that lacks some required graphics libraries.
#
FROM alpine

ARG OPENJDK_VERSION="21"

RUN apk add --no-cache openjdk${OPENJDK_VERSION}-jre

# Setup the link before the files are in place, because we don't have permission after we change user.
RUN ln -s /Drawj2d/drawj2d /usr/local/bin/drawj2d

RUN adduser -D app
USER app

COPY --from=builder /build/Drawj2d/dist /Drawj2d
