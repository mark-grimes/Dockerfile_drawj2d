# Dockerfile for [drawj2d](https://drawj2d.sourceforge.io)

Downloads the java code from Sourceforge, builds with ant and then puts it in a container.

I am not affilated with drawj2d in any way, I just find it easier to use in a docker container.
If you like drawj2d consider donating to it at the link on the homepage.

There is no entrypoint because drawj2d is often used by piping input into it from `echo`, and
that's awkward directly to a docker container. Note that the base image is alpine which only
has the `sh` shell installed by default.

## Creating reMarkable notebooks from diagrams

I use drawj2d for converting PDF diagrams into reMarkable notebooks. To do this you would
pipe the command "image my_diagram.pdf" into drawj2d with you output requirements, e.g.

```
echo "image my_diagram.pdf" | drawj2d -Trmapi -o my_diagram.zip
```

As a docker one-liner this would be:

```
docker run --rm -it -v /path/to/my/plots:/work --workdir /work sh -c 'echo "image my_diagram.pdf" | drawj2d -Trmapi -o my_diagram.zip'
```

You can then use a program like [rmapi](https://github.com/juruen/rmapi) to get the file onto
the reMarkable. Apparently you can also use (RCU)[http://www.davisr.me/projects/rcu/] but I've
never tried it. Note that you need different outputs depending on which you intend to use:

* For rmapi, use the output format `-Trmapi`
* For RCU, use the output format `-Trmn`
* For the official reMarkable desktop app, use the output format `-Trmdoc` and give the filename the ".rmdoc" extension.

For more information see the [official documentation](https://sourceforge.net/p/drawj2d/wiki/reMarkable/).
